import React from 'react'
import ToolbarHeader from '../js/features/editor-navigation-toolbar/components/toolbar-header'

export const Default = () => {
  return <ToolbarHeader />
}

export default {
  title: 'EditorNavigationToolbar'
}
